from django.shortcuts import render
from . import views
from .models import *

# Importacion para los mensajes de confirmacion.
from django.contrib import messages
#LIBRERIAS PARA HACER EL CRUD DE LA FOTO
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
# Create your views here.

def index(request):
    return render(request, 'index.html')
